# Season Ticket Manager

This project is to provide visibility and analytics for selling a full set of season tickets on StubHub.

##

1. Provide a list of home games from MLB data: https://www.mlb.com/redsox/schedule/downloadable-schedule

1. Allow user to add meta-data to each game:
    - flag to indicate if game will be sold, gifted, or retained
    - name of attendee for gifted or retained
    - pricing tier
    - face value of ticket

1. Use integration with StubHub to provide additional meta data: https://developer.stubhub.com/
    - update current selling price
    - mark a ticket as sold with the sell date
    - provide visibility into current pricing for nearby seats currently for sale
    - update StubHub with adjusted sale price for tickets

## Core Technology

This project is written in TypeScript and React, using the Next.js framework for development and delivery. The UI is built using Ant Design system of components. Data is stored in PostgreSQL, using the Prisma toolkit for data modelling, migrations and ORM.