export interface GameData {
    key: string,
    date: string,
    time: string,
    description: string,
    location: string
}