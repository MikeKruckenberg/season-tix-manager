import Head from 'next/head'
import Image from 'next/image'
import styles from './layout.module.css'
import Link from 'next/link'

export const siteTitle = 'Red Sox Ticket Manager'

export default function Layout({
    children,
    home
  } : {
    children: React.ReactNode,
    home?: boolean
  }) {
  return (
    <div className={styles.container}>
      <Head>
        <meta name="og:title" content={siteTitle} />
      </Head>
      <header className={styles.header}>
          <>
            <Image
                src="/images/red-sox-logo.jpg"
                height={144}
                width={144}
                alt="Boston Red Sox"
            />
            <h1 className="title">
                Sox Season Ticket Manager
            </h1>

          </>
      </header>
      <main>{children}</main>
      {!home && (
        <div className={styles.backToHome}>
          <Link href="/">
            <a>← Back to home</a>
          </Link>
        </div>
      )}
    </div>
  )
}
