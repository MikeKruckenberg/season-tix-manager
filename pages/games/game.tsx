import Head from 'next/head'
import Link from 'next/link'
import Layout from '../../components/layout'

export default function Game() {
    return (
        <Layout>
            <Head>
                <title>Red Sox Game</title>
            </Head>
            <h1>Game</h1>
            <Link href="/">
                <a>home</a>
            </Link>
        </Layout>
    )
}