import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import { getSortedGamesData } from '../lib/games'
import { GetStaticProps } from 'next'
import Link from 'next/link'
import { GameData } from '../components/types'
import { EditOutlined } from '@ant-design/icons'
import { Table } from 'antd'
import "antd/dist/antd.css";

export const getStaticProps: GetStaticProps = async () => {
  const allGamesData = await getSortedGamesData()
  return {
    props: {
      allGamesData
    }
  }
}

const gameColumns = [
  {
    title: 'Date',
    key: 'date',
    dataIndex: 'date'
  },
  {
    title: 'Time',
    key: 'time',
    dataIndex: 'time'
  },
  {
    title: 'Description',
    key: 'description',
    dataIndex: 'description'
  },
  {
    title: 'Location',
    key: 'location',
    dataIndex: 'location'
  },
  {
    title: 'Tier',
    key: 'tier',
    dataIndex: 'tier'
  },
  {
    title: 'Face value',
    key: 'face_value',
    dataIndex: 'face_value'
  },
  {
    title: 'Resell',
    key: 'resell',
    dataIndex: 'resell'
  },
  {
    title: 'Resell price',
    key: 'resell_price',
    dataIndex: 'resell_price'
  },
  {
    title: 'Reciepient',
    key: 'recipient',
    dataIndex: 'recipient'
  },
  {
    title: 'Actions',
    key: 'action',
    render: (text, record) => (
      <Link href="/games/game">
        <EditOutlined />
      </Link>
    )
  }
];

export default function Home( { allGamesData } : { allGamesData: GameData[] } ) {
  console.log(allGamesData,'data');

  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <Table dataSource={allGamesData} columns={gameColumns} />
    </Layout>
  )
}