import fs from 'fs'
import path from 'path'
import csv from 'csv-parser'
import { GameData } from '../components/types';

const seasonYear = '2022'
const gamesCSVFileName = 'sox-home-games.csv'
const dataDirectory = path.join(process.cwd(), 'data/')

export function getSortedGamesData() {
  // Get CSV game data
  const csvFile = dataDirectory + seasonYear + '-' + gamesCSVFileName
  let allGamesData: GameData[] = [];

  let fd = fs.createReadStream(csvFile)
    .pipe(csv())
    .on('data',function(data) {
      if (data['LOCATION'].indexOf('Fenway Park') === -1) return;
      allGamesData.push({
        key: data['START DATE']+data['START TIME'],
        date: data['START DATE'],
        time: data['START TIME'],
        description: data['SUBJECT'],
        location: data['LOCATION']
      })
    })
  // resolve when the data is loaded
  return new Promise(function(resolve, reject) {
    fd.on('end', () => { resolve(allGamesData) })
    fd.on('error', reject)
  })
}
